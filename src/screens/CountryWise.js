import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import axios from 'axios'
import { SearchBar } from 'react-native-elements';
import CountryWiseComp from '../components/CountryWiseComp'

class CountryWise extends Component {
	constructor(props) {
		super(props)
		this.state = {
			countrywiseData: [],
			errorText: '',
			search: '',
			showIndicator: true
		}
	}

	componentDidMount() {
		this._getcovidData()
	}


	_getcovidData = async () => {
		await axios.get('https://coronavirus-19-api.herokuapp.com/countries')
			.then(res => {
				console.log("countrys data-->>",res.data[0])
				this.setState({
					countrywiseData: res.data,
					showIndicator: false,
				})
			})
			.catch(
				err => console.log(err)
			)
	}

	updateSearch = search => {
		this.setState({ search });
	};

	render() {
		const { countrywiseData, search, showIndicator } = this.state
		const filterdData = countrywiseData.filter((p) => {
			return (
				p.country.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
			)
		})

		if (showIndicator) {
			return (
				<View style={styles.loaderContainer}>
					<ActivityIndicator size="large" color="#32aeb1" />
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					<SearchBar
						lightTheme
						cancelIcon
						searchIcon
						placeholder="Search by Country name..."
						onChangeText={this.updateSearch}
						value={search}
					/>
					<CountryWiseComp countrywiseData={filterdData} />
				</View>
			)
		}
	}
}

export default CountryWise

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f2f2f2'
	},
	loaderContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
})
import React, { Component } from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import axios from 'axios'
import { SearchBar } from 'react-native-elements';
import ContinentComp from '../components/ContinentComp'

class ContinentData extends Component {
  constructor(props) {
    super(props)
    this.state = {
      continentData: [],
      erorrText: '',
      showIndicator: true,
      search: '',
      // continentName:[]
    }
  }

  componentDidMount() {
    this._getDistrictData()
  }



  _getDistrictData = async () => {
    await axios.get(`https://corona.lmao.ninja/v2/continents?yesterday=true&sort`)
      .then(res => {
        this.setState({
          continentData: res.data,
          showIndicator: false,
          // continentName:res.data.continent
        })
      })
      .catch(err => console.log(err))
  }


  updateSearch = search => {
		this.setState({ search });
	};

  render() {
    const { continentData, showIndicator, search } = this.state
    const filterdData = continentData.filter((p) => {
			return (
				p.continent.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
			)
		})


    if (showIndicator) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#32aeb1" />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <SearchBar
						lightTheme
						cancelIcon
						searchIcon
						placeholder="Search by Country name..."
						onChangeText={this.updateSearch}
						value={search}
					/>
          <ContinentComp continentData={filterdData} />
        </View>
      )
    }
  }
}

export default ContinentData

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  inputField: {
    borderColor: 'red',
    borderWidth: 1,
    height: 40
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
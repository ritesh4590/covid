import React, { Component } from 'react'
import { ScrollView, View, TouchableOpacity, Text, StyleSheet, ToastAndroid,ActivityIndicator, BackHandler, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux';
import axios from 'axios'
import News from '../components/svg/News'
import Countrywise from '../components/svg/Countrywise'
import HelpLineSVG from '../components/svg/HelpLineSVG'
import IndiasStateSVG from '../components/svg/IndiasStateSVG'
import GlobalDataSVG from '../components/svg/GlobalDataSVG'
import ContinentSVG from '../components/svg/ContinentSVG'

export class LandingPage extends Component {
	constructor(props) {
		super(props)
		this.state = {
			covidData: [],
			errorText: '',
			showIndicator: true
		}
	}

 

	componentDidMount() {
		this._getcovidDataLandingPage()
	}

	_getcovidDataLandingPage = () => {
		axios.get('https://coronavirus-19-api.herokuapp.com/countries')
			.then(res => {
				console.log("landing page data--->>>",res)
				this.setState({
					covidData: res.data[0],
					showIndicator: false
				})
			})
			.catch(
				err => console.log(err),
				this.setState({
					errorText: "Something went wrong"
				})
			)
	}

	render() {
		const { covidData, showIndicator } = this.state

		if (showIndicator) {
			return (
				<View style={styles.loaderContainer}>
					<ActivityIndicator size="large" color="#32aeb1" />
				</View>
			);
		} else {
			return (

				<ScrollView style={styles.container}>
					<View style={styles.singleCountry}>
						<View style={styles.countryNameView}>
							<Text style={styles.countryName}>Global Cases</Text>
						</View>
						<View style={styles.statsdata}>
							<View style={styles.caseView}>
								<Text style={[styles.statsHeading, { color: '#8C7672' }]}>Cases</Text>
								<Text style={styles.statsCount}>{covidData.cases}</Text>
							</View>
							<View style={styles.caseView}>
								<Text style={[styles.statsHeading, { color: '#5ED565' }]}>Recovered</Text>
								<Text style={styles.statsCount}>{covidData.recovered}</Text>
							</View>
							<View style={styles.caseView}>
								<Text style={[styles.statsHeading, { color: '#F92200' }]}>Deaths</Text>
								<Text style={styles.statsCount}>{covidData.deaths}</Text>
							</View>
						</View>
					</View>

					<View style={styles.LandingRow}>
						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.countrywise()} style={styles.BoxOne}>
							<Countrywise />
							<Text style={styles.BoxText}>World Covid Data</Text>
						</TouchableOpacity>
						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.statewise()} style={styles.BoxOne}>
							<IndiasStateSVG />
							<Text style={styles.BoxText}>India Covid Data</Text>
						</TouchableOpacity>
					</View>

					<View style={styles.LandingRow}>
						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.globalStatsScreen()} style={styles.BoxOne}>
							<GlobalDataSVG />
							<Text style={styles.BoxText}>Global Data</Text>
						</TouchableOpacity>

						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.news()} style={styles.BoxOne}>
							<News />
							<Text style={styles.BoxText}>Latest News</Text>
						</TouchableOpacity>

					</View>

					<View style={styles.LandingRow}>
						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.helpline()}
							style={styles.BoxOne}>
							<HelpLineSVG />
							<Text style={styles.BoxText}>HelpLine Number</Text>
						</TouchableOpacity>

						<TouchableOpacity activeOpacity={.5} onPress={() => Actions.continentdata()}
							style={styles.BoxOne}>
							<ContinentSVG />
							<Text style={styles.BoxText}>Continent Wise Data</Text>
						</TouchableOpacity>

					</View>
				</ScrollView>
			)
		}
	}
}

export default LandingPage

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f2f2f2'
	},
	loaderContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	homeGlobal: {
		backgroundColor: "#32aeb1",
		height: 120,
		marginTop: 15,
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10
	},
	singleCountry: {
		backgroundColor: '#ffffff',
		marginTop: 15,
		marginLeft: 10,
		marginRight: 10,
		paddingTop: 10,
		paddingBottom: 20,
		paddingLeft: 10,
		paddingRight: 10,
		borderRadius: 12,

	},
	countryNameView: {
		textAlign: 'center'
	},
	countryName: {
		textAlign: 'center',
		fontSize: 18,
		fontFamily: 'Nexa Bold'
	},
	statsdata: {
		marginTop: 5,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	statsCount: {
		color: '#B8B3B2',
		fontSize: 14,
		fontFamily: 'Nexa Regular'
	},
	caseView: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	statsHeading: {
		fontSize: 16,
		margin: 5,
		fontFamily: 'Nexa Regular'

	},
	LandingRow: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		marginTop: 15
	},
	BoxOne: {
		height: 140,
		width: '45%',
		backgroundColor: '#ffffff',
		borderRadius: 5,
		justifyContent: 'center',
		alignItems: 'center'
	},
	BlankBox: {
		height: 140,
		width: '45%',
	},
	BoxText: {
		color: '#000',
		fontFamily: "Nexa Regular",
		marginTop: 10
	}

})
import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator, } from 'react-native'
import axios from 'axios'
import StateWiseData from '../components/StateWiseData'
import { SearchBar } from 'react-native-elements';

class StateWise extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateData: [],
      search: '',
      showIndicator: true,
      time:false
    }
  }

  componentDidMount() {
    this._getStateData()
    setTimeout(() => {
      this.setState({time: true})
  }, 100)
  }



  _getStateData = async() => {
    await axios.get('https://api.covid19india.org/data.json')
      .then(res => {
        this.setState({
          stateData: res.data.statewise,
          showIndicator: false,
        })
      })
      .catch(err => console.log(err))
  }

  updateSearch = search => {
    this.setState({ search });
  };



  render() {
    const { stateData, search, showIndicator } = this.state

    const filterdData = stateData.filter((p) => {
      return (
        p.state.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
      )
    })

    if (showIndicator) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#32aeb1" />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <SearchBar
            lightTheme
            cancelIcon
            searchIcon
            placeholder="Search by State name..."
            onChangeText={this.updateSearch}
            value={search}
          />
          <StateWiseData stateData={filterdData} />
        </View>
      )
    }
  }
}

export default StateWise

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  inputField: {
    borderColor: 'red',
    borderWidth: 1,
    height: 40
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
import React, { Component } from 'react'
import { View,  StyleSheet, ActivityIndicator  } from 'react-native'
import axios from 'axios'
import GlobalStats from '../components/GlobalStats'

class GlobalStatsScreen extends Component {
	constructor(props) {
		super(props)
		this.state = {
			covidData: [],
			errorText: '',
			showIndicator:true
		}
	}

	componentDidMount() {
		this._getcovidData()
	}


	_getcovidData = async() => {
	await	axios.get('https://thevirustracker.com/free-api?global=stats')
			.then(res => {
				this.setState({
					covidData: res.data.results,
					showIndicator:false
				})
			})
			.catch(
				err => 
				this.setState({
						errorText:"Something went wrong"
				})
			)
	}

	render() {
		const { covidData, showIndicator, errorText } = this.state

		if(showIndicator){
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#32aeb1" />
        </View>
      );  
    }else{
		return (
			<View style={styles.container}>
				<GlobalStats covidData={covidData} errorText={errorText}/>
			</View>
		)
		}
	}
}

export default GlobalStatsScreen

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f2f2f2'
	},
	loaderContainer:{
		flex:1,
		justifyContent:'center',
		alignItems:'center'
	}
})
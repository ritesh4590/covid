import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, StyleSheet } from 'react-native';
import axios from 'axios'
import NewsComp from '../components/NewsComp'

const AppKey = '7d3afcf1555149b39ab0a392d4e7016d'
class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
      refreshing: true,
      showIndicator:true
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  fetchNews = async () => {
    await axios.get(`https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=${AppKey}`)
      .then(res => {
        this.setState({
          articles: res.data.articles ,
          showIndicator:false,
          refreshing: false
        })
      })
      .catch(() => this.setState({ refreshing: false }))
  }

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => this.fetchNews()
    );
  }

  render() {
    const { showIndicator } = this.state

    if(showIndicator){
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#32aeb1" />
        </View>
      );  
    }else{
    return (
      <>
        <FlatList
          data={this.state.articles}
          renderItem={({ item }) => <NewsComp article={item} />}
          keyExtractor={item => item.url}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
        />
      </>
    );
    }
  }
}

export default News

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
	loaderContainer:{
		flex:1,
		justifyContent:'center',
		alignItems:'center'
	}
})
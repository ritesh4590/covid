import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator, BackHandler, Alert } from 'react-native'
import axios from 'axios'
import { SearchBar } from 'react-native-elements';
import HelplineComp from '../components/HelplineComp'


class Helpline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helpLineData: [],
      helplineNumber: [],
      search: '',
      showIndicator: true
    }
  }

  componentDidMount() {
    this._getHelplineNo()
  }

  _getHelplineNo = async () => {
    await axios.get('https://covid-19india-api.herokuapp.com/v2.0/helpline_numbers')
      .then(res => {
        this.setState({
          helpLineData: res.data[1],
          helplineNumber: res.data[1].contact_details,
          showIndicator: false
        })
      })
      .catch(err => console.log(err))
  }


  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { helpLineData, helplineNumber, search, showIndicator } = this.state

    const filterdData = helplineNumber.filter((p) => {
      return (
        p.state_or_UT.toLowerCase().indexOf(search.toLowerCase()) !== -1
      )
    })

    if (showIndicator) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#32aeb1" />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <SearchBar
            lightTheme
            cancelIcon
            searchIcon
            placeholder="Search by State name..."
            onChangeText={this.updateSearch}
            value={search}
          />
          <HelplineComp
            helplineNumber={filterdData}
            helpLineData={helpLineData}
          />
        </View>
      )
    }
  }
}

export default Helpline

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
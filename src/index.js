import React, { Component } from 'react'
import { StatusBar, BackHandler, ToastAndroid, } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import GlobalStatsScreen from './screens/GlobalStatsScreen'
import LandingPage from './screens/LandingPage'
import {Actions, Router, Scene } from 'react-native-router-flux'
import Header from './components/Header'
import CountryWise from './screens/CountryWise'
import StateWise from '../src/screens/StateWise'
import Helpline from '../src/screens/Helpline'
import News from './screens/News'
import ContinentData from '../src/screens/ContinentData'


let backPressed = 0;
export class Index extends Component {
	constructor(props) {
		super(props)
		this.state = {
			backPressed: 1

		}
	}
	
	componentDidMount() {
		SplashScreen.hide();
		BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
	if(Actions.currentScene === 'landingPage'){
			if(backPressed > 0){
					BackHandler.exitApp();
					backPressed = 0;
			}else {
					backPressed++;
					ToastAndroid.show("Press Again To Exit", ToastAndroid.SHORT);
					setTimeout( () => { backPressed = 0}, 2000);
					return true;
			}
	}
}

	render() {
		console.disableYellowBox = true;
		return (
			<>
				<StatusBar
					barStyle="light-content"
					backgroundColor="#32aeb1"
				/>
				<Header />
				<Router>
					<Scene key="root">
						<Scene initial key="landingPage" component={LandingPage} hideNavBar />
						<Scene key="globalStatsScreen" component={GlobalStatsScreen} hideNavBar />
						<Scene key="countrywise" component={CountryWise} hideNavBar />
						<Scene key="statewise" component={StateWise} hideNavBar />
						<Scene key="helpline" component={Helpline} hideNavBar />
						<Scene key="news" component={News} hideNavBar />
						<Scene key="continentdata" component={ContinentData} hideNavBar />
						
					</Scene>
				</Router>
			</>
		)
	}
}

export default Index

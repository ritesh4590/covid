import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet  } from 'react-native'
 
import { Actions } from 'react-native-router-flux';


class CountryWiseComp extends Component {
	constructor(props) {
		super(props)
		this.state = {
			countrydetails: []
		}
	}

	componentDidMount() {
		this._setStatedata()
	}

	_setStatedata = () => {
		this.setState({
			countrydetails: this.props.countrywiseData
		})
	}

	render() {
		return (
			<ScrollView>
				{
					this.props.countrywiseData.map((stats, key) =>
						<View onPress={() => Actions.countrydetails()} key={stats.country} style={styles.singleCountry}>
							<View style={styles.countryNameView}>
								<Text style={styles.countryName}>{stats.country}</Text>
							</View>
							<View style={styles.statsdata}>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading,{color:'#8C7672'}]}>Cases</Text>
									<Text style={styles.statsCount}>{stats.cases}</Text>
								</View>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading,{color:'#5ED565'}]}>Recovered</Text>
									<Text style={styles.statsCount}>{stats.recovered}</Text>
								</View>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading,{color:'#F92200'}]}>Deaths</Text>
									<Text style={styles.statsCount}>{stats.deaths}</Text>
								</View>
							</View>
						</View>
					)
				}

			</ScrollView>
		)
	}
}

export default CountryWiseComp

const styles = StyleSheet.create({
	singleCountry: {
		backgroundColor:'#ffffff',
		margin: 8,
		paddingTop:10,
		paddingBottom:20,
		paddingLeft:10,
		paddingRight:10,
		borderRadius:12
	},
	countryNameView: {
		textAlign: 'center'
	},
	countryName: {
		textAlign: 'center',
		fontSize:18,
		fontFamily:'Nexa Bold'
	},
	statsdata: {
		marginTop:5,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	statsCount:{
		color:'#B8B3B2',
		fontSize:14,
		fontFamily:'Nexa Regular'
	},
	caseView:{
		justifyContent:'center',
		alignItems:'center',
	},
	statsHeading:{
		fontSize:16,
		margin:5,
		fontFamily:'Nexa Regular'

	}
})


{/* <Text>{stats.todayCases}</Text> */ }
{/* <Text>{stats.todayDeaths}</Text> */ }
{/* <Text>{stats.active}</Text> */ }
{/* <Text>{stats.critical}</Text> */ }
{/* <Text>{stats.casesPerOneMillion}</Text> */ }
{/* <Text>{stats.deathsPerOneMillion}</Text> */ }
{/* <Text>{stats.totalTests}</Text> */ }
{/* <Text>{stats.testsPerOneMillion}</Text> */ }
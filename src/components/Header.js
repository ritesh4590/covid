import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux';
import Back from '../components/svg/Back'
class Header extends Component {
	render() {
		return (
			<View style={styles.container}>
				<TouchableOpacity activeOpacity={.5} onPress={() => Actions.pop()}>
					<Back />
				</TouchableOpacity>
				<View style={styles.headerView}>
					<Text style={styles.headerText}>COVID-19 Stats</Text>
				</View>
				<View></View>
			</View>
		)
	}
}

export default Header


const styles = StyleSheet.create({
	container: {
		backgroundColor: "#32aeb1",
		height: 50,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		padding: 10
	},
	headerText: {
		fontFamily: 'Nexa Bold',
		color: '#ffffff',
		fontSize: 20,
		lineHeight: 25
	},

})
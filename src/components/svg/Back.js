import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path } from 'react-native-svg';


class Back extends React.Component {
	render() {
		return (
			<View>
				<Svg width="20px" height="20px" viewBox="0 0 57 60" version="1.1">
					<G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="05-Create-New-Listing" transform="translate(-60.000000, -283.000000)" fill="#FFFFFF" fillRule="nonzero">
							<Path d="M93.2814256,283.884548 C94.4447226,285.059353 94.439498,286.95884 93.2697561,288.127172 L70.899,310.47 L113.980459,310.000177 C115.630075,309.982169 116.981889,311.310637 117,312.967393 C117.01775,314.62415 115.695008,315.981815 114.045391,315.999823 L71.983,316.458 L93.2773865,337.880477 C94.4429223,339.053041 94.441321,340.952535 93.2738098,342.123116 C92.1062986,343.293696 90.2149903,343.292088 89.0494545,342.119523 L60,312.89489 L89.0570849,283.872828 C90.2268268,282.704496 92.1181285,282.709743 93.2814256,283.884548 Z" id="Back"></Path>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default Back
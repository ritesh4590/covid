import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'

class ContinentComp extends Component {
  render() {
    return (
      <ScrollView>
        {
          this.props.continentData.map((data, key) =>
          <View key={data.state} style={styles.singleCountry}>
							<View style={styles.countryNameView}>
								<Text style={styles.countryName}>{data.continent}</Text>
							</View>

							<View style={styles.statsdata}>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading, { color: '#8C7672' }]}>Cases</Text>
									<Text style={styles.statsCount}>{data.cases}</Text>
								</View>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading, { color: '#5ED565' }]}>Recovered</Text>
									<Text style={styles.statsCount}>{data.recovered}</Text>
								</View>
								<View style={styles.caseView}>
									<Text style={[styles.statsHeading, { color: '#F92200' }]}>Deaths</Text>
									<Text style={styles.statsCount}>{data.deaths}</Text>
								</View>
							</View>
						</View>
            // <View key={data.cases}>
            //   <Text>{data.continent}</Text>
            //   <Text>{data.cases}</Text>
            //   <Text>{data.deaths}</Text>
            //   <Text>{data.recovered}</Text>
            //   <Text>{ data.countries }</Text>
            //   <Text>{'\n'}</Text>
            // </View>
          )
        }
      </ScrollView>
    )
  }
}

export default ContinentComp

const styles = StyleSheet.create({
	singleCountry: {
		backgroundColor: '#ffffff',
		margin: 8,
		paddingTop: 10,
		paddingBottom: 20,
		paddingLeft: 10,
		paddingRight: 10,
		borderRadius: 12
	},
	countryNameView: {
		textAlign: 'center'
	},
	countryName: {
		textAlign: 'center',
		fontSize: 18,
		fontFamily: 'Nexa Bold'
	},
	statsdata: {
		marginTop: 5,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	statsCount: {
		color: '#B8B3B2',
		fontSize: 14,
		fontFamily: 'Nexa Regular'
	},
	caseView: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	statsHeading: {
		fontSize: 16,
		margin: 5,
		fontFamily: 'Nexa Regular'

	}
})
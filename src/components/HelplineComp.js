import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'

class HelplineComp extends Component {
  render() {
    const { helpLineData, helplineNumber } = this.props
    return (
      <ScrollView>
        <View style={styles.singleCountry}>
          <View style={styles.countryNameView}>
            <Text style={styles.countryName}>Central Government</Text>
          </View>
          <View style={styles.statsdata}>
            <View style={styles.caseView}>
              <Text style={styles.statsHeading}>Helpline No</Text>
              <Text style={styles.statsCount}>{helpLineData.helpline_number}</Text>
            </View>
            <View style={styles.caseView}>
              <Text style={styles.statsHeading}>Helpline Email</Text>
              <Text style={styles.statsCount}>{helpLineData.helpline_email}</Text>
            </View>
            <View style={styles.caseView}>
              <Text style={styles.statsHeading }>Helpline Toll-Free</Text>
              <Text style={styles.statsCount}>{helpLineData.toll_free}</Text>
            </View>
          </View>
        </View>
        {
          helplineNumber.map((data, key) =>
            <View key={data.state_or_UT} style={styles.singleCountry}>
              <View style={styles.countryNameView}>
                <Text style={styles.countryName}>{data.state_or_UT}</Text>
              </View>
              <View style={styles.statsdata}>
                <View style={styles.caseView}>
                  <Text style={[styles.statsHeading, { color: '#000' }]}>Helpline No</Text>
                  <Text style={styles.statsCount}>{data.helpline_number}</Text>
                </View>
              </View>
            </View>
          )
        }
      </ScrollView>
    )
  }
}

export default HelplineComp

const styles = StyleSheet.create({
  singleCountry: {
    backgroundColor: '#ffffff',
    margin: 8,
    paddingTop: 10,
    borderRadius: 12,
    borderColor: '#B8B3B2',
    borderWidth: 1,
  },
  countryNameView: {
    paddingTop: 8,
    paddingBottom: 10,
    alignItems:'center',
  },
  countryName: {
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Nexa Bold',
    width:'85%' 
  },
  statsdata: {
    justifyContent: 'space-around'
  },
  statsCount: {
    color: '#2E2C2C',
    fontSize: 14,
    fontFamily: 'Nexa Regular'
  },
  caseView: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#B8B3B2',
    borderTopWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  statsHeading: {
    fontSize: 16,
    margin: 5,
    fontFamily: 'Nexa Regular',
    color: '#8C7672'
  }
})
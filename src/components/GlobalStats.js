import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView, ActivityIndicator } from 'react-native'

class GlobalStats extends Component {
	render() {
		const { covidData, errorText } = this.props
		return (
			<ScrollView>
				{covidData.map((data, key) =>
					<View key={data}>
						<View style={styles.mainRow}>
							<View style={[styles.statsOne, { backgroundColor: '#fdb4c5' }]}>
								<Text style={[styles.headingText, { color: '#fb0035' }]}>Confirmed</Text>
								<Text style={[styles.stats, { color: '#fb0035' }]}>
									{data.total_cases}
								</Text>
							</View>
							<View style={[styles.statsOne, { backgroundColor: '#b1d7fe' }]}>
								<Text style={[styles.headingText, { color: '#117af9' }]}>Active </Text>
								<Text style={[styles.stats, { color: '#117af9' }]}>
									{data.total_active_cases}
								</Text>
							</View>
						</View>

						<View style={styles.mainRow}>
							<View style={[styles.statsOne, { backgroundColor: '#bde4c7' }]}>
								<Text style={[styles.headingText, { color: '#23a543' }]}>Recovered </Text>
								<Text style={[styles.stats, { color: '#23a543' }]}>
									{data.total_recovered}
								</Text>
							</View>

							<View style={[styles.statsOne, { backgroundColor: '#d4d5d9' }]}>
								<Text style={[styles.headingText, { color: '#69727b' }]}>Deaths</Text>
								<Text style={[styles.stats, { color: '#69727b' }]}>
									{data.total_deaths}
								</Text>
							</View>
						</View>

						<View style={styles.mainRow}>
							<View style={[styles.statsOne, { backgroundColor: '#ebccff' }]}>
								<Text style={[styles.headingText, { color: '#5c00e6' }]}>New Cases</Text>
								<Text style={[styles.stats, { color: '#5c00e6' }]}>
									{data.total_new_cases_today}
								</Text>
							</View>

							<View style={[styles.statsOne, { backgroundColor: '#b3ffff' }]}>
								<Text style={[styles.headingText, { color: '#00ccff' }]}>New deaths</Text>
								<Text style={[styles.stats, { color: '#00ccff' }]}>
									{data.total_new_deaths_today}
								</Text>
							</View>
						</View>

						<View style={styles.mainRow}>
							<View style={[styles.statsAffectedCountry, { backgroundColor: '#cccc99' }]}>
								<Text style={[styles.headingText, { color: '#666633' }]}>Affected Countries</Text>
								<Text style={[styles.stats, { color: '#666633' }]}>
									{data.total_affected_countries}
								</Text>
							</View>
						</View>
					</View>
				)
				}
				{errorText ? <View><Text>sss</Text></View>:null}


			</ScrollView>
		)
	}
}

export default GlobalStats

const styles = StyleSheet.create({
	headingText: {
		fontFamily: 'Nexa Bold',
		fontSize: 20,
		fontWeight: 'bold'
	},
	stats: {
		fontFamily: 'Nexa Bold',
		fontSize: 25,
		fontWeight: 'bold'
	},
	mainRow: {
		backgroundColor: '#ffffff',
		height: 'auto',
		padding: 10,
		justifyContent: 'space-between',
		flexDirection: 'row'
	},
	statsOne: {
		height: 120,
		borderRadius: 10,
		width: '47%',
		justifyContent: 'center',
		alignItems: "center"
	},
	statsAffectedCountry: {
		height: 120,
		borderRadius: 10,
		width: '100%',
		justifyContent: 'center',
		alignItems: "center"
	}
})